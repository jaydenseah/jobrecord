<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Job Record System | Citytrans Logistics</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../system/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../system/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php SESSION_START(); ?>
  <!-- Top Navbar @ top_nav.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/top_nav.php'); ?>

  <!-- Left Sidebar @ left_sidebar.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/left_sidebar.php'); ?>

  <!-- Contents @ index_contents.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/contents/home.php'); ?>

  <!-- Right Sidebar @ right_sidebar.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/right_sidebar.php'); ?>

  <!-- Footer @ footer.php -->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/footer.php'); ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../../system/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../system/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../system/dist/js/adminlte.min.js"></script>
</body>
</html>
