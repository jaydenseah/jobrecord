// Script mainly for add-record.php
// Will add new function for other file as well

// Main function when page loaded
$(function() {
  readRecord(); //Read Main Record
  readCRUD();
  readMYData(); //Read MY Company when loading
  readSGData(); //Read SG Company when loading
  readTruckData();

  // Datepicker
  $('#jobDate').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayBtn: "linked",
    todayHighlight: true,
  });

  // Selectpicker adjust
  $('.selectpicker').selectpicker();

  $('input[name="dateRange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

  $('input[name="dateRange2"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
});


// Space for sessionstorage
window.onload = function() {
  var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
  var eventName = isOnIOS ? "pagehide" : "beforeunload";

  // If sessionStorage is storing default values (ex. name), exit the function and do not restore data
  if (sessionStorage.getItem('jobDate') == "jobDate") {
      return;
  }

  // If values are not blank, restore them to the fields
  var jobDate = sessionStorage.getItem('jobDate');
  var jobNo = sessionStorage.getItem('jobNo');
  var smkReg = sessionStorage.getItem('smkReg');
  if (jobDate !== null) $('#jobDate').val(jobDate);
  if (jobNo !== null) $('#jobNo').val(jobNo);
  if (smkReg !== null) $('#smkReg').val(smkReg);

  // Before refreshing the page, save the form data to sessionStorage
  window.addEventListener(eventName, function (event) {
    sessionStorage.setItem("jobDate", $('#jobDate').val());
    sessionStorage.setItem("jobNo", $('#jobNo').val());
    sessionStorage.setItem("smkReg", $('#smkReg').val());
  })
}
//end for sessionstorage

// Start CRUD for Malaysia Company
function addMy() {
  var myName = $("#myName").val();

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      myName:myName
    },
    success:function(response) {
      if (response.trim() == 'registered') {
        swal("Error!", myName + " has added, please check.", "error");
      } else {
        swal("Success", myName + " added successfully.", "success");
        $('#myTable').DataTable().ajax.reload(); //reload the table
      }
    }
  });
}

function readMYData() {
  var readMy = "readMy";

  $('#myTable').DataTable({
      'ajax' : {
        "type": "POST",
        "url": "../../system/process/manage_company.php",
        "data":{
          readMy:readMy
        },
        "dataSrc": function (json) {
          var return_data = new Array();
            for(var i=0;i< json.length; i++){
              return_data.push({
                'id' : json[i].id,
                'myName' : json[i].myName
              })
            }
            return return_data;
          }
        },

      "columns": [
        {'data': 'id'},
        {'data': 'myName'},
        {
          mRender: function(data,type,row) {
            return '<a href="#modal-updateMy" data-toggle="modal" onclick="updateMy('+row['id']+')"><i class="fas fa-pen"></i> Update</a> | <a href="#" onclick="deleteMy('+row['id']+')"><i class="fas fa-trash-alt"></i> Delete</a>';
          }
        }
      ]
  });
}

function updateMy(id) {
  var uid = id;

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      uid:uid
    },
    success:function(data,status) {
      var res = $.parseJSON(data);
      console.log(res);
      $("#modal-updateMy").find("input[name='uMyName']").val(res[0]['myName']);
      $("#modal-updateMy").find("input[name='hidden_user_id']").val(res[0]['id']); //to show id in hidden
    },
  });
  console.log("ID is: "+uid)
}

function deleteMy(id) {
  swal({
    title: "Delete",
    text: "Do you want to delete this record?",
    icon: "warning",
    buttons: {
      cancel: true,
      confirm: "Confirm",
    },

    }).then(function (isConfirm) {
    if (isConfirm == true) {
      $.ajax({
        url:"../../system/process/manage_company.php",
        type: "POST",
        data: {
          id : id
        },

        success:function(data, status){
          console.log(id + " deleted.");
          swal("Success", "Record has been deleted!", "success");
          $('#myTable').DataTable().ajax.reload(); //reload the table
        },

         error: function (xhr, ajaxOptions, thrownError) {
          swal("Error deleting!", "Please try again", "error");
        }
      });
    } else {
      swal("Cancelled", "Selected record is not deleted.", "error");
    }
  });
}

function auMySelect() {
  var form = JSON.stringify($('#modal-updateMy #updateMyForm').serializeArray());

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      form
    },
    success:function(data,status) {
      swal("Success", "Record has been updated!", "success");
      $('#modal-updateMy').modal('toggle');
      $('#myTable').DataTable().ajax.reload(); //reload the table
    },
  });
}

// End CRUD for Malaysia Company

// Start CRUD for SG Company
function readSGData() {
  var readSg = "readSg";

  $('#sgTable').DataTable({
      'ajax' : {
        "type": "POST",
        "url": "../../system/process/manage_company.php",
        "data":{
          readSg:readSg
        },
        "dataSrc": function (json) {
          var return_data = new Array();
            for(var i=0;i< json.length; i++){
              return_data.push({
                'id' : json[i].id,
                'sgName' : json[i].sgName
              })
            }
            return return_data;
          }
        },

      "columns": [
        {'data': 'id'},
        {'data': 'sgName'},
        {
          mRender: function(data,type,row) {
            return '<a href="#modal-updateSg" data-toggle="modal" onclick="updateSg('+row['id']+')"><i class="fas fa-pen"></i> Update</a> | <a href="#" onclick="deleteSg('+row['id']+')"><i class="fas fa-trash-alt"></i> Delete</a>';
          }
        }
      ]
  });
}

function addSg() {
  var sgName = $("#sgName").val();

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      sgName:sgName
    },
    success:function(response) {
      if (response.trim() == 'registered') {
        swal("Error!", sgName + " has added, please check.", "error");
      } else {
        swal("Success", sgName + " added successfully.", "success");
        $('#sgTable').DataTable().ajax.reload(); //reload the table
      }
    }
  });
}

function deleteSg(id) {
  swal({
    title: "Delete",
    text: "Do you want to delete this record?",
    icon: "warning",
    buttons: {
      cancel: true,
      confirm: "Confirm",
    },

    }).then(function (isConfirm) {
    if (isConfirm == true) {
      $.ajax({
        url:"../../system/process/manage_company.php",
        type: "POST",
        data: {
          id : id
        },

        success:function(data, status){
          console.log(id + " deleted.");
          swal("Success", "Record has been deleted!", "success");
          $('#sgTable').DataTable().ajax.reload(); //reload the table
        },

         error: function (xhr, ajaxOptions, thrownError) {
          swal("Error deleting!", "Please try again", "error");
        }
      });
    } else {
      swal("Cancelled", "Selected record is not deleted.", "error");
    }
  });
}

function updateSg(id) {
  var usid = id;

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      usid:usid
    },
    success:function(data,status) {
      var res = $.parseJSON(data);
      console.log(res);
      $("#modal-updateSg").find("input[name='uSgName']").val(res[0]['sgName']);
      $("#modal-updateSg").find("input[name='hidden_user_usid']").val(res[0]['id']); //to show id in hidden
    },
  });
  console.log("ID is: "+usid)
}

function auSgSelect() {
  var form = JSON.stringify($('#modal-updateSg #updateSgForm').serializeArray());

  $.ajax({
    url: "../../system/process/manage_company.php",
    type: 'POST',
    data: {
      form
    },
    success:function(data,status) {
      swal("Success", "Record has been updated!", "success");
      $('#modal-updateSg').modal('toggle');
      $('#sgTable').DataTable().ajax.reload(); //reload the table
    },
  });
}
// End CRUD for Singapore Company

// CRUD for Truck Select
function addTruck() {
  var truckInput = $("#truckInput").val();

  $.ajax({
    url: "../../system/process/manage_truck.php",
    type: 'POST',
    data: {
      truckInput:truckInput
    },
    success:function(response) {
      if (response.trim() == 'registered') {
        swal("Error!", truckInput + " has added, please check.", "error");
      } else {
        swal("Success", truckInput + " added successfully.", "success");
        $('#truckTable').DataTable().ajax.reload(); //reload the table
        $('#manageTruck')[0].reset();
      }
    }
  });
}

function editTruck() {
  var form = JSON.stringify($('#modal-updateTruck #updateTruckForm').serializeArray());

  $.ajax({
    url: "../../system/process/manage_truck.php",
    type: 'POST',
    data: {
      form
    },
    success:function(data,status) {
      swal("Success", "Record has been updated!", "success");
      $('#modal-updateTruck').modal('toggle');
      $('#truckTable').DataTable().ajax.reload(); //reload the table
    },
  });
}

function deleteTruck(id) {
  swal({
    title: "Delete",
    text: "Do you want to delete this record?",
    icon: "warning",
    buttons: {
      cancel: true,
      confirm: "Confirm",
    },

    }).then(function (isConfirm) {
    if (isConfirm == true) {
      $.ajax({
        url:"../../system/process/manage_truck.php",
        type: "POST",
        data: {
          id : id
        },

        success:function(data, status){
          console.log(id + " deleted.");
          swal("Success", "Record has been deleted!", "success");
          $('#truckTable').DataTable().ajax.reload(); //reload the table
        },

         error: function (xhr, ajaxOptions, thrownError) {
          swal("Error deleting!", "Please try again", "error");
        }
      });
    } else {
      swal("Cancelled", "Selected record is not deleted.", "error");
    }
  });
}

function readTruckData() {
  var readTruck = "readTruck";

  $('#truckTable').DataTable({
      'ajax' : {
        "type": "POST",
        "url": "../../system/process/manage_truck.php",
        "data":{
          readTruck:readTruck
        },
        "dataSrc": function (json) {
          var return_data = new Array();
            for(var i=0;i< json.length; i++){
              return_data.push({
                'id' : json[i].id,
                'lorryNo' : json[i].lorryNo
              })
            }
            return return_data;
          }
        },

      "columns": [
        {'data': 'id'},
        {'data': 'lorryNo'},
        {
          mRender: function(data,type,row) {
            return '<a href="#modal-updateTruck" data-toggle="modal" onclick="updateTruck('+row['id']+')"><i class="fas fa-pen"></i> Update</a> | <a href="#" id="deleteTruck" name="deleteTruck" onclick="deleteTruck('+row['id']+')"><i class="fas fa-trash-alt"></i> Delete</a>';
          }
        }
      ]
  });
}

function updateTruck(id) {
  var utid = id;

  $.ajax({
    url: "../../system/process/manage_truck.php",
    type: 'POST',
    data: {
      utid:utid
    },
    success:function(data,status) {
      var res = $.parseJSON(data);
      console.log(res);
      $("#modal-updateTruck").find("input[name='uTruck']").val(res[0]['lorryNo']);
      $("#modal-updateTruck").find("input[name='hidden_user_tid']").val(res[0]['id']); //to show id in hidden
    },
  });
  console.log("ID is: "+utid);
}
// End CRUD for Truck

//this readRecord serves as VIEW RECORD table
function readRecord() {
  var readMain = "readMain";
  var recordTable; //for datatable defining
  var start = moment();
  var end = moment();

  function cb(start, end) {
    $('#dateRange2 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  }

  $('#dateRange2').daterangepicker({
    startDate: start,
    endDate: end,
    autoApply: true,
    alwaysShowCalendars: true,
    locale: {
      format: "YYYY-MM-DD",
      cancelLabel: "Clear",
      separator: " to ",
    },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }, cb);

  cb(start, end);

  $('#dateRange2').on('apply.daterangepicker', function(ev, picker) {
    var start = picker.startDate;
    var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[1]);

        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    recordTable.draw();
    $.fn.dataTable.ext.search.pop();
  });

  recordTable = $('#recordTable').DataTable({
    "bPaginate": true,
    "searching": true,
    "sorting": true,
    "dom": 'Bfrtip',
    buttons: ['excel'],
    // "columnDefs": [{
    //     "targets":[0],
    //     "visible":false,
    //     "searchable":false,
    //   }],
    'ajax' : {
      "type": "POST",
      "url": "../../system/process/manage_main.php",
      "data":{
        readMain:readMain
      },
        "dataSrc": function (json) {
          var return_data = new Array();
            for(var i=0;i< json.length; i++){
              return_data.push({
                //'id' : json[i].id,
                'jobDate' : json[i].jobDate,
                'lorryNo' : json[i].lorryNo,
                'station' : json[i].station,
                'smkReg' : json[i].smkReg,
                'consignee' : json[i].consignee,
                'consignor' : json[i].consignor,
                'jobType' : json[i].jobType,
                'handlingMode' : json[i].handlingMode,
                'remark' : json[i].remark,
              })
            }
            return return_data;
          }
        },
      "columns": [
        //{'data': 'id'},
        {'data': 'jobDate'},
        {'data': 'lorryNo'},
        {'data': 'station'},
        {'data': 'smkReg'},
        {'data': 'consignee'},
        {'data': 'consignor'},
        {'data': 'jobType'},
        {'data': 'handlingMode'},
        {'data': 'remark'}
      ]
  });
}
// End for View Record

// JS for Add RECORD
function readCRUD() {
  var crudRec = "crudRec"; //this one pass into ajax
  var crudTable; //defining datatable
  var start = moment();
  var end = moment();

  function cb(start, end) {
    $('#dateRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  }

  $('#dateRange').daterangepicker({
    startDate: start,
    endDate: end,
    autoApply: false,
    alwaysShowCalendars: true,
    locale: {
      format: "YYYY-MM-DD",
      cancelLabel: "Clear",
      separator: " to ",
    },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }, cb);

  cb(start, end);

  $('#dateRange').on('apply.daterangepicker', function(ev, picker) {
    var start = picker.startDate;
    var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[1]);

        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    crudTable.draw();
    $.fn.dataTable.ext.search.pop();
  });

  crudTable = $('#crudTable').DataTable({
    "bPaginate": true,
    "searching": true,
    "sorting": false,
    "order": [[ 1, "asc" ]],
    "dom": 'Bfrtip',
    buttons: ['excel'],
    "columnDefs": [{
        "targets":[0],
        "visible":false,
        "searchable":false,
      }],
    'ajax' : {
      "type": "POST",
      "url": "../../system/process/manage_main.php",
      "data":{
        crudRec:crudRec
      },
        "dataSrc": function (json) {
          var return_data = new Array();
            for(var i=0;i< json.length; i++){
              return_data.push({
                'id' : json[i].id,
                'jobDate' : json[i].jobDate,
                'lorryNo' : json[i].lorryNo,
                'station' : json[i].station,
                'smkReg' : json[i].smkReg,
                'consignee' : json[i].consignee,
                'consignor' : json[i].consignor,
                'jobType' : json[i].jobType,
                'handlingMode' : json[i].handlingMode,
                'remark' : json[i].remark
              })
            }
            return return_data;
          }
        },
      "columns": [
        {'data': 'id'},
        {'data': 'jobDate'},
        {'data': 'lorryNo'},
        {'data': 'station'},
        {'data': 'smkReg'},
        {'data': 'consignee'},
        {'data': 'consignor'},
        {'data': 'jobType'},
        {'data': 'handlingMode'},
        {'data': 'remark'},
        {
          mRender: function(data,type,row) {
            return '<a href="#modal-updateMain" data-toggle="modal" onclick="updateRecord('+row['id']+')"><i class="fas fa-pen"></i> Update</a> | <a href="#" onclick="deleteRecord('+row['id']+')"><i class="fas fa-trash-alt"></i> Delete</a>';
          }
        }
      ]
  });
}

// Start MAIN Record
function insertRecord() {
  var insert = JSON.stringify($('#insertForm').serializeArray());
  console.log(insert);

  $.ajax({
    url: "../../system/process/manage_main.php",
    type: 'POST',
    data: {
      insert
    },
    success:function(response) {
      if (response.trim() == 'error') {
        swal("Error!", "Some error occured. Contact webadmin!", "error");
      } else {
        swal("Success", "Record added successfully.", "success");
        $('#crudTable').DataTable().ajax.reload(); //reload the table
        $('#insertForm')[0].reset();
        localStorage.clear();
        location.reload(true);
      }
    }
  });
}

function updateRecord(id) {
  var umid = id;
  console.log("ID is: "+umid);

  $.ajax({
    url: "../../system/process/manage_main.php",
    type: 'POST',
    data: {
      umid:umid
    },
    success:function(data,status) {
      var res = $.parseJSON(data);
      console.log(res);
      //
        // cekNo: "250001"
        // consignee: "ABC"
        // consignor: "ARKEMA"
        // handlingMode: "HC"
        // id: "2"
        // jobDate: "2020-05-19"
        // jobID: "2"
        // jobType: "K1"
        // lorryNo: "BLJ8794"
        // remark: "nil"
        // smkReg: "J10105001000"
        // station: "J10"
      //
      $("#modal-updateMain").find("input[name='ujobDate']").val(res[0]['jobDate']);
      $("#modal-updateMain").find("input[name='uTruckNo']").val(res[0]['lorryNo']);
      $("#modal-updateMain").find("input[name='uSMKReg']").val(res[0]['smkReg']);
      $("#modal-updateMain").find("input[name='uConsignee']").val(res[0]['consignee']);
      $("#modal-updateMain").find("input[name='uConsignor']").val(res[0]['consignor']);
      $("#modal-updateMain").find("input[name='ujobType']").val(res[0]['jobType']);
      $("#modal-updateMain").find("input[name='uhandlingMode']").val(res[0]['handlingMode']);
      $("#modal-updateMain").find("input[name='uRemark']").val(res[0]['remark']);
      $("#modal-updateMain").find("input[name='hidden_user_umid']").val(res[0]['jobID']); //to show id in hidden
    },
  });
}

function sendUpdate() {
  var updateform = JSON.stringify($('#modal-updateMain #updateMainForm').serializeArray());
  console.log(updateform);

  $.ajax({
    url: "../../system/process/manage_main.php",
    type: 'POST',
    data: {
      updateform
    },
    success:function(data,status) {
      swal("Success", "Record has been updated!", "success");
      $('#modal-updateMain').modal('toggle');
      $('#crudTable').DataTable().ajax.reload(); //reload the table
    },
  });
}

function deleteRecord(id) {
  swal({
    title: "Delete",
    text: "Do you want to delete this record?",
    icon: "warning",
    buttons: {
      cancel: true,
      confirm: "Confirm",
    },

    }).then(function (isConfirm) {
    if (isConfirm == true) {
      $.ajax({
        url:"../../system/process/manage_main.php",
        type: "POST",
        data: {
          id : id
        },

        success:function(data, status){
          console.log(id + " deleted.");
          swal("Success", "Record has been deleted!", "success");
          $('#crudTable').DataTable().ajax.reload(); //reload the table
        },

         error: function (xhr, ajaxOptions, thrownError) {
          swal("Error deleting!", "Please try again", "error");
        }
      });
    } else {
      swal("Cancelled", "Selected record is not deleted.", "error");
    }
  });
}
