<!-- include profile for suer config -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/system/includes/profile.php'); ?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="../../system/home.php" class="brand-link">
    <img src="https://citytrans.my/img/logo.png" alt="AdminLTE Logo" style="opacity: .8; width:220px; height:50px;">
    <span class="brand-text font-weight-light"></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo $transformAva; ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?php echo $username; ?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="../../system/home.php" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Home
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-database"></i>
            <p>
              Job Record
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="../../system/add-record.php" class="nav-link">
                <i class="fas fa-plus-circle nav-icon"></i>
                <p>Add New Record</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../../system/view-record.php" class="nav-link">
                <i class="fas fa-file nav-icon"></i>
                <p>View Record</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="../../system/logout.php" class="nav-link">
            <i class="nav-icon fas fa-key"></i>
            <p>
              Logout
            </p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
              User Level =  echo $level; ?>
            </p>
          </a>
        </li> -->
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
