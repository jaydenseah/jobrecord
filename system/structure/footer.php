<?php include($_SERVER['DOCUMENT_ROOT'].'/system/includes/properties.php'); ?>

<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">
    <!-- leave it for future purpose -->
  </div>
  <!-- Default to the left -->
  <?php echo "Copyright &copy; <strong>" . $companyName . "</strong>, " . $copyrightYear . ". All rights reserved." ?>
</footer>
