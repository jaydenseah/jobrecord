<?php
//crud mainly for main record
//easier and cleaner look perhaps?
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/system/includes/db.php');
include($_SERVER['DOCUMENT_ROOT'] . '/system/includes/profile.php');

extract($_POST);

switch (true) {
  case isset($_POST['insert']):
    $json = $_POST['insert'];
    $arr = (json_decode($json, true)); //this to decode array
    //[{"name":"jobDate","value":""},'
    //'{"name":"truckNo","value":"BHM9246"},
    //{"name":"smkReg","value":""},
    //{"name":"consignee","value":"3REES"},
    //{"name":"consignor","value":"AGRI SUPPLIES"},
    //{"name":"jobType","value":"K1"},
    //{"name":"handlingMode","value":"HC"},
    //{"name":"remarkTxt","value":""}]
    $jobDate = $arr[0]["value"];
    $truckNo = $arr[1]["value"];
    $smkReg = $arr[2]["value"];
    $consignee = $arr[3]["value"];
    $consignor = $arr[4]["value"];
    $jobType = $arr[5]["value"];
    $handlingMode = $arr[6]["value"];
    $remark = $arr[7]["value"];

    //$query = "INSERT INTO lorrySelect(lorryNo) VALUES ('$truckNo')";
    //first to insert into jobhead first
    $firstquery = "INSERT INTO jobhead(jobDate, lorryNo) values ('$jobDate', '$truckNo')";

    //run $firstquery
    $runfirst = mysqli_query($conn,$firstquery);
    $lastID = mysqli_insert_id($conn); //use this to get last id

    //here to extract Station
    $station = substr($smkReg, 0, 3);
    $upperStation = strtoupper($station);
    $upperSMK = strtoupper($smkReg);

    $finalquery = "INSERT INTO jobdetails(jobID, station, smkReg, consignee, consignor, jobType, handlingMode, remark, user) values ('$lastID', '$upperStation', '$upperSMK', '$consignee', '$consignor', '$jobType', '$handlingMode', '$remark', '$username')";

    if ($result = mysqli_query($conn,$finalquery)) {
      echo "success";
    } else {
      $errormsg = "error";
    }
  break;

  case isset($_POST['crudRec']):

    if ($level == "3") {
      $readquery = "SELECT * FROM jobhead AS A left join ( SELECT jobID, GROUP_CONCAT(DISTINCT(station) SEPARATOR '+') AS station, GROUP_CONCAT(DISTINCT(smkReg) SEPARATOR '+') AS smkReg, GROUP_CONCAT(DISTINCT(consignee) SEPARATOR '+') AS consignee, GROUP_CONCAT(DISTINCT(consignor) SEPARATOR '+') AS consignor, GROUP_CONCAT(DISTINCT(jobType) SEPARATOR '+') AS jobType, GROUP_CONCAT(DISTINCT(handlingMode) SEPARATOR '+') AS handlingMode, GROUP_CONCAT(DISTINCT(remark) SEPARATOR '+') AS remark FROM jobdetails GROUP BY jobID ) AS B ON A.id = B.jobID";
      $result = mysqli_query($conn, $readquery);

      while($row = mysqli_fetch_assoc($result)) {
        $crudArr[] = $row;
      }

      echo json_encode($crudArr);

    } else if ($level == "2") {
      $readquery = "SELECT * FROM jobhead AS A left join ( SELECT jobID, GROUP_CONCAT(DISTINCT(station) SEPARATOR '+') AS station, GROUP_CONCAT(DISTINCT(smkReg) SEPARATOR '+') AS smkReg, GROUP_CONCAT(DISTINCT(consignee) SEPARATOR '+') AS consignee, GROUP_CONCAT(DISTINCT(consignor) SEPARATOR '+') AS consignor, GROUP_CONCAT(DISTINCT(jobType) SEPARATOR '+') AS jobType, GROUP_CONCAT(DISTINCT(handlingMode) SEPARATOR '+') AS handlingMode, GROUP_CONCAT(DISTINCT(remark) SEPARATOR '+') AS remark FROM jobdetails GROUP BY jobID ) AS B ON A.id = B.jobID where station = 'J25'";
      $result = mysqli_query($conn, $readquery);

      while($row = mysqli_fetch_assoc($result)) {
        $crudArr[] = $row;
      }

      echo json_encode($crudArr);
    }
  break;

  case isset($_POST['readMain']):
    if ($level == "2") {
      $readquery = "SELECT jobDate, lorryNo, station, GROUP_CONCAT(DISTINCT(smkReg) SEPARATOR ' + ') as smkReg, GROUP_CONCAT(DISTINCT(consignee) SEPARATOR ' + ') as consignee, GROUP_CONCAT(DISTINCT(consignor) SEPARATOR ' + ') as consignor, GROUP_CONCAT(DISTINCT(jobType) SEPARATOR ' + ') as jobType, GROUP_CONCAT(DISTINCT(handlingMode) SEPARATOR ' + ') as handlingMode, GROUP_CONCAT(DISTINCT(remark) SEPARATOR ' + ') as remark from (select A.jobDate, A.lorryNo, B.station, B.smkReg, B.consignee, B.consignor, B.jobType, B.handlingMode, B.remark from jobhead as A inner join jobdetails as B on A.id = B.jobID where station='J25') X group by jobDate, lorryNo, station";

      $result = mysqli_query($conn, $readquery);

      while($row = mysqli_fetch_assoc($result)) {
        $mainArr[] = $row;
      }

      echo json_encode($mainArr);
    } else if ($level == "3") {
      $readquery = "SELECT jobDate, lorryNo, station, GROUP_CONCAT(DISTINCT(smkReg) SEPARATOR ' + ') as smkReg, GROUP_CONCAT(DISTINCT(consignee) SEPARATOR ' + ') as consignee, GROUP_CONCAT(DISTINCT(consignor) SEPARATOR ' + ') as consignor, GROUP_CONCAT(DISTINCT(jobType) SEPARATOR ' + ') as jobType, GROUP_CONCAT(DISTINCT(handlingMode) SEPARATOR ' + ') as handlingMode, GROUP_CONCAT(DISTINCT(remark) SEPARATOR ' + ') as remark from (select A.jobDate, A.lorryNo, B.station, B.smkReg, B.consignee, B.consignor, B.jobType, B.handlingMode, B.remark from jobhead as A inner join jobdetails as B on A.id = B.jobID) X group by jobDate, lorryNo, station";

      $result = mysqli_query($conn, $readquery);

      while($row = mysqli_fetch_assoc($result)) {
        $mainArr[] = $row;
      }

      echo json_encode($mainArr);
    }
  break;

  case isset($_POST['umid']):
    $id = $_POST['umid'];

    $query = "SELECT jobhead.jobDate, jobhead.lorryNo, jobdetails.jobID, jobdetails.smkReg, jobdetails.consignee, jobdetails.consignor, jobdetails.jobType, jobdetails.handlingMode, jobdetails.remark from jobhead inner join jobdetails on jobhead.id = jobdetails.id where jobdetails.jobID = '$id'";
    $res = mysqli_query($conn,$query);

    while($row = mysqli_fetch_assoc($res)) {
  		$updateArr[] = $row;
  	}

  	echo json_encode($updateArr);
  break;

  case isset($_POST['updateform']):
    //[{"name":"hidden_user_umid","value":"2"},{"name":"ujobDate","value":"2020-06-02"},{"name":"uTruckNo","value":"BLQ5134"},{"name":"uCekNo","value":"250001"},{"name":"uSMKReg","value":"J10105001010"},{"name":"uConsignee","value":"CIPC"},{"name":"uConsignor","value":"TPSC"},{"name":"ujobType","value":"K1"},{"name":"uhandlingMode","value":"HC"},{"name":"uRemark","value":""}]
    $json = $_POST['updateform'];
    $arr = (json_decode($json, true));
    //change to update field name :>
    $jobID = $arr[0]["value"];
    $jobDate = $arr[1]["value"];
    $lorryNo = $arr[2]["value"];
    $smkReg = $arr[3]["value"];
    $consignee = $arr[4]["value"];
    $consignor = $arr[5]["value"];
    $jobType = $arr[6]["value"];
    $handlingMode = $arr[7]["value"];
    $remark = $arr[8]["value"];

    $upperTruck = strtoupper($lorryNo);
    $upperMY = strtoupper($consignee);
    $upperSG = strtoupper($consignor);

    $station = substr($smkReg, 0, 3);

    $q1 = "UPDATE jobdetails SET station='$station', smkReg='$smkReg', consignee='$upperMY', consignor='$upperSG', jobType='$jobType', handlingMode='$handlingMode', remark='$remark' where jobID = '$jobID'";

    $q2 = "UPDATE jobhead SET jobDate='$jobDate', lorryNo='$upperTruck' WHERE id = '$jobID'";

    if ($result = mysqli_query($conn,$q1) && $res = mysqli_query($conn,$q2) == TRUE) {
      echo "Done";
    } else {
      echo "Failed";
    }
  break;

  case isset($_POST['id']):
    $id = $_POST['id'];
    $dq1 = "delete from jobdetails where jobID ='$id'";
    $dq2 = "delete from jobhead where id='$id'";

    if ($result = mysqli_query($conn,$dq1) && $res = mysqli_query($conn,$dq2) == TRUE) {
      echo "Done";
    } else {
      echo "Failed";
    }
  break;
}
?>
