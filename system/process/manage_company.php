<?php

include($_SERVER['DOCUMENT_ROOT'] . '/system/includes/db.php');

//extract what's inside post
extract($_POST);

// Start for Malaysia Company
if(isset($_POST['readMy'])){
	//Query
  $query = "SELECT * from myselect";
  $r = mysqli_query($conn,$query);

	while($row = mysqli_fetch_assoc($r)) {
		$myArr[] = $row;
	}

	echo json_encode($myArr);
}

//Adding new entry
if(isset($_POST['myName'])) {
	$name = strtoupper($myName);
  $check = "SELECT * from myselect WHERE myName = '$name'";
  $checkRes = mysqli_query($conn, $check);
  $rowcheck = mysqli_num_rows($checkRes);

  $errormsg = '';

  if ($rowcheck > 0) {
    $errormsg = "registered";
    echo $errormsg;
  } else {
    $query = "INSERT INTO myselect(myName) VALUES ('$name')";
    if ($result = mysqli_query($conn,$query)) {
      echo "success";
    } else {
      $errormsg = "error";
    }
  }
}

//Delete entry
if(isset($_POST['id'])) {
	$id = $_POST['id'];
	$deletequery = "delete from myselect where id ='$id'";

	if (!$result = mysqli_query($conn,$deletequery)) {
    	echo "delete failed";
	}
}

//Update by id
if(isset($_POST['uid'])) {
	//Query
	$uid = $_POST['uid'];
  $uquery = "SELECT * from myselect where id='$uid'";
  $u = mysqli_query($conn,$uquery);

	while($urow = mysqli_fetch_assoc($u)) {
		$updateArr[] = $urow;
	}

	echo json_encode($updateArr);
}


//update text
if(isset($_POST['form'])) {
	$json = $_POST['form'];
	$arr = (json_decode($json, true));
	// $arr[0]["value"]; //this shows id (but this is not needed)
	$id = $arr[0]["value"];
	$myName = $arr[1]["value"]; //this shows companyname
  $upperMY = strtoupper($myName);

	$query = "UPDATE mySelect SET myName = '$upperMY' where id = '$id'";
  if (!$result = mysqli_query($conn,$query)) {
      exit(mysqli_error());
	} else {
		echo "New name: ".$upperMY; //shows new name
	}
}
// End for Malaysia Company

// Start for Singapore Company
if(isset($_POST['readSg'])){
	//Query
  $query = "SELECT * from sgselect";
  $r = mysqli_query($conn,$query);

	while($row = mysqli_fetch_assoc($r)) {
		$sgArr[] = $row;
	}

	echo json_encode($sgArr);
}

//Adding new entry
if(isset($_POST['sgName'])) {
  $name = strtoupper($sgName);
  $check = "SELECT * from sgselect WHERE sgName = '$name'";
  $checkRes = mysqli_query($conn, $check);
  $rowcheck = mysqli_num_rows($checkRes);

  $errormsg = '';

  if ($rowcheck > 0) {
    $errormsg = "registered";
    echo $errormsg;
  } else {
    $query = "INSERT INTO sgselect(sgName) VALUES ('$name')";
    if ($result = mysqli_query($conn,$query)) {
      echo "success";
    } else {
      $errormsg = "error";
    }
  }
}

//delete by id
if(isset($_POST['id'])) {
	$id = $_POST['id'];
	$deletequery = "delete from sgselect where id ='$id'";

	if (!$result = mysqli_query($conn,$deletequery)) {
    	echo "delete failed";
	}
}

//Update by id
if(isset($_POST['usid'])) {
	//Query
	$usid = $_POST['usid'];
    $uquery = "SELECT * from sgselect where id='$usid'";
    $u = mysqli_query($conn,$uquery);

	while($urow = mysqli_fetch_assoc($u)) {
		$updateArr[] = $urow;
	}

	echo json_encode($updateArr);
}

if(isset($_POST['form'])) {
	$json = $_POST['form'];
	$arr = (json_decode($json, true));
	// $arr[0]["value"]; //this shows id (but this is not needed)
	$id = $arr[0]["value"];
	$sgName = $arr[1]["value"]; //this shows companyname
  $upperSG = strtoupper($sgName);

	$query = "UPDATE sgselect SET sgName = '$upperSG' where id = '$id'";
    if (!$result = mysqli_query($conn,$query)) {
        exit(mysqli_error());
  	} else {
  		echo "New name: ".$upperSG; //shows new name
  	}
}
?>
