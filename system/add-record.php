<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Add Record | Citytrans Logistics</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../system/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../system/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DatePicker CSS-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.standalone.min.css">
  <!-- selectpicker -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../system/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../system/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../../system/plugins/daterangepicker/daterangepicker.css">
  <!-- SWAL -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.13/dist/sweetalert2.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php session_start(); ?>

  <!-- Select Process -->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/process/select_process.php'); ?>

  <!-- Top Navbar @ top_nav.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/top_nav.php'); ?>

  <!-- Left Sidebar @ left_sidebar.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/left_sidebar.php'); ?>

  <!-- Contents @ index_contents.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/contents/addrecord.php'); ?>

  <!-- Right Sidebar @ right_sidebar.php-->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/right_sidebar.php'); ?>

  <!-- Footer @ footer.php -->
  <?php include($_SERVER['DOCUMENT_ROOT'].'/system/structure/footer.php'); ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../../system/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../system/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SelectPicker -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<!-- AdminLTE App -->
<script src="../../system/dist/js/adminlte.min.js"></script>
<!-- Custom JS -->
<script src="../../system/dist/js/custom.js"></script>
<!-- moment.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Date Picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<!-- DataTables -->
<script src="../../system/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../system/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../system/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../system/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/pdfmake.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/vfs_fonts.js"></script>
<!-- date-range-picker -->
<script src="../../system/plugins/daterangepicker/daterangepicker.js"></script>
</body>
</html>
