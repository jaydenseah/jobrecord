<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Login | Citytrans Logistics</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../system/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../system/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Swal -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body class="hold-transition login-page">
  <?php
    require($_SERVER['DOCUMENT_ROOT'] . '/system/includes/db.php');
    session_start();

    //if form submitted, insert value into db
    if (isset($_POST['username'])) {
      //remove backlashes and escape special char
      $username = stripslashes($_REQUEST['username']);
      $username = mysqli_real_escape_string($conn,$username);
      $password = stripslashes($_REQUEST['password']);
      $password = mysqli_real_escape_string($conn,$password);

      //Checking is user existing in the database or not
      $query = "SELECT * FROM `users` WHERE username='$username' and password='".md5($password)."'";
      $result = mysqli_query($conn,$query) or die(mysql_error());
      $rows = mysqli_num_rows($result);

      if ($rows==1) {
        $_SESSION['username'] = $username;
        // Redirect user to index.php
        header("location:../../system/home.php");
      } else {
  ?>
        <script>
          swal({
            title: "Invalid Username/Password!",
            text: "Please key in the correct details.",
            icon: "error",
            button: "Ok",
          });
        </script>
  <?php
      }
    }
  ?>

  <div class="login-box">
    <div class="login-logo">
      <a href="../../index.php">Citytrans Logistics</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <?php
          if (!isset($_SESSION['username'])) {
            echo '<p class="login-box-msg">Sign in to start your session</p>';
          } else {
            header("location:../../system/home.php");
          }
        ?>

        <form action="" method="post" id="login" name="login">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Username" id="username" name="username">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block">Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../../system/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../system/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../system/dist/js/adminlte.min.js"></script>
</body>
</html>
