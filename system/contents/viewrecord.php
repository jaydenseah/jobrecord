<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">View Record</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="../../system/home.php">Home</a></li>
            <li class="breadcrumb-item active">View Record</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-eye"></i> &nbsp;View Table</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-3">
                  <!-- Filter Range -->
                </div>
              </div>

              <hr />

              <div class="row">
                <div class="col-md-12">
                  <table id="recordTable" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                        <th>Job Date</th>
                        <th>Lorry No</th>
                        <th>Station</th>
                        <th>Job No</th>
                        <th>Consignee</th>
                        <th>Consignor</th>
                        <th>Job Type</th>
                        <th>Handling Mode</th>
                        <th>Remark</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- row -->
    </div><!-- /.container-fluid -->
  </div><!-- content end -->
  <!-- /.content -->
</div>
