<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Add New Job</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="../../system/home.php">Home</a></li>
            <li class="breadcrumb-item active">Add New Job</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-pen"></i> &nbsp;Add New Record</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                <div class="btn-group">
                  <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fas fa-th"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right"role="menu">
                    <a data-toggle="modal" data-target="#modal-manageTruck" style="color:black;" class="dropdown-item"><i class="fas fa-truck"></i> Manage Truck</a>
                    <a data-toggle="modal" data-target="#modal-manageMY" style="color:black;" class="dropdown-item"><i class="fas fa-warehouse"></i> Manage Consignee (MY)</a>
                    <a data-toggle="modal" data-target="#modal-manageSG" style="color:black;" class="dropdown-item"><i class="fas fa-warehouse"></i> Manage Consignor (SG)</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="card-body">
              <form role="form" action="" method="post" id="insertForm" name="insertForm">
                <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>Date</label>
                        <input type="text" class="form-control pull-right" id="jobDate" name="jobDate" placeholder="Date" autocomplete="off">
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Truck No</label>
                      <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="truckNo" id="truckNo">
                      <?php foreach ($arraytruck as $optiontruck):?>
                        <option value="<?php echo $optiontruck->lorryNo; ?>"><?php echo $optiontruck->lorryNo;?></option>
                      <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-7">
                    <div class="form-group">
                      <label>SMK Register <small><i>(eg, J10105001000 + J10105001001)</i></small></label>
                        <input type="text" class="form-control pull-right" id="smkReg" name="smkReg" placeholder="Job No">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Consignee (MY)</label>
                      <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="consignee" id="consignee">
                      <?php foreach ($arraymy as $optionmy):?>
                        <option value="<?php echo $optionmy->myName; ?>"><?php echo $optionmy->myName;?></option>
                      <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Consignor (SG)</label>
                        <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="consignor" id="consignor">
                          <?php foreach ($arraysg as $optionsg):?>
                            <option value="<?php echo $optionsg->sgName; ?>"><?php echo $optionsg->sgName;?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Job Type</label>
                      <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="jobType" id="jobType">
                        <option value="K1">K1</option>
                        <option value="K2">K2</option>
                        <option value="K8">K8</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>HC & OT</label>
                      <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="handlingMode" id="handlingMode">
                        <option value="HC">HC</option>
                        <option value="HC & OT">HC & OT</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Remark</label>
                      <input type="text" class="form-control" placeholder="Remark" name="remarkTxt" id="remarkTxt">
                    </div>
                  </div>

                  <p><small><i>
                    * Remark column is for recording company name, eg, Citytrans, Peninsula. <br />
                    * Citytrans holds EZ Kitchen, Ledang Health. <br />
                    * Peninsula holds Sun Fresh, Naylor Foods.
                  </i></small></p>
                </div>
            </div> <!-- body ends here -->

            <div class="card-footer">
              <button type="button" class="btn btn-primary float-right" onclick="insertRecord()">Add New</button>
              </form><!-- FORM ENDS HERE -->
            </div>
          </div>
        </div>


        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-eye"></i> &nbsp;View Table</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control pull-right" id="dateRange" name="dateRange">
                  </div>
                </div>
              </div>

              <hr />

              <div class="row">
                <div class="col-md-12">
                  <table id="crudTable" class="table table-bordered table-striped" style="width:100%">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Job Date</th>
                        <th>Lorry No</th>
                        <th>Station</th>
                        <th>Job No</th>
                        <th>Consignee</th>
                        <th>Consignor</th>
                        <th>Job Type</th>
                        <th>Handling Mode</th>
                        <th>Remark</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div> <!-- body ends here -->
          </div>
        </div> <!-- end view -->
      </div><!-- row -->
    </div><!-- /.container-fluid -->
  </div><!-- content end -->
  <!-- /.content -->

  <!-- Modal starts here -->
  <!-- Manage Truck -- >
  <!-- Manage Malaysia -->
  <div class="modal fade" id="modal-manageTruck">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Manage Truck</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="javascript:window.location.reload()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="manageTruck" name="manageTruck" method="post">
                <div class="form-group">
                  <label>Enter Truck:</label>
                  <input type="text" class="form-control" placeholder="Enter Truck No" name="truckInput" id="truckInput">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="addTruck()">Add New</button>
                </div>
              </form>
            </div>
          </div>

          <hr />

          <div class="row">
            <div class="col-md-12">
              <table id="truckTable" class="table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Truck</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Manage Malaysia -->
  <div class="modal fade" id="modal-manageMY">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Manage Consignee (MY)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="javascript:window.location.reload()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="manageMY" name="manageMY" method="post">
                <div class="form-group">
                  <label>Enter Malaysia Company Name:</label>
                  <input type="text" class="form-control" placeholder="Enter Malaysia Company" name="myName" id="myName">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="addMy()">Add New</button>
                </div>
              </form>
            </div>
          </div>

          <hr />

          <div class="row">
            <div class="col-md-12">
              <table id="myTable" class="table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Manage Singapore -->
  <div class="modal fade" id="modal-manageSG">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Manage Consignor (SG)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="javascript:window.location.reload()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="manageSG" name="manageSG" method="post">
                <div class="form-group">
                  <label>Enter Singapore Company Name:</label>
                  <input type="text" class="form-control" placeholder="Enter Singpaore Company" name="sgName" id="sgName">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="addSg()">Save</button>
                </div>
              </form>
            </div>
          </div>

          <hr />

          <div class="row">
            <div class="col-md-12">
              <table id="sgTable" class="table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Update Truck modal -->
  <div class="modal fade" id="modal-updateTruck">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Update Truck</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="updateTruckForm" name="updateTruckForm" method="post">
                <div class="form-group">
                  <label>Update Malaysia Company Name:</label>
                  <input type="hidden" id="hidden_user_tid" name="hidden_user_tid">
                  <input type="text" class="form-control" name="uTruck" id="uTruck">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="editTruck()">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Update Malaysia modal -->
  <div class="modal fade" id="modal-updateMy">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Update Consignor (MY)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="updateMyForm" name="updateMyForm" method="post">
                <div class="form-group">
                  <label>Update Malaysia Company Name:</label>
                  <input type="hidden" id="hidden_user_id" name="hidden_user_id">
                  <input type="text" class="form-control" name="uMyName" id="uMyName">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="auMySelect()">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Update Singapore modal -->
  <div class="modal fade" id="modal-updateSg">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Update Consignee (SG)</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="updateSgForm" name="updateSgForm" method="post">
                <div class="form-group">
                  <label>Update Singapore Company Name:</label>
                  <input type="hidden" id="hidden_user_usid" name="hidden_user_usid">
                  <input type="text" class="form-control" name="uSgName" id="uSgName">
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="auSgSelect()">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Update Main Record modal -->
  <div class="modal fade" id="modal-updateMain">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-database"></i> &nbsp;&nbsp;Update Record</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <form action="" id="updateMainForm" name="updateMainForm" method="post">
                <div class="form-group">
                  <input type="hidden" id="hidden_user_umid" name="hidden_user_umid"> <!-- This one for hidden_user_id-->
                  <!--
                    cekNo: "250001"
                    consignee: "ABC"
                    consignor: "ARKEMA"
                    handlingMode: "HC"
                    id: "2"
                    jobDate: "2020-05-19"
                    jobID: "2"
                    jobType: "K1"
                    lorryNo: "BLJ8794"
                    remark: "nil"
                    smkReg: "J10105001000"
                    station: "J10"
                  -->
                  <div class="form-group">
                    <label>Job Date</label>
                    <input type="text" class="form-control" name="ujobDate" id="ujobDate">
                  </div>

                  <div class="form-group">
                    <label>Truck No</label>
                    <input type="text" class="form-control" name="uTruckNo" id="uTruckNo">
                  </div>

                  <div class="form-group">
                    <label>SMK Reg</label>
                    <input type="text" class="form-control" name="uSMKReg" id="uSMKReg">
                  </div>

                  <div class="form-group">
                    <label>Update Consignee (MY):</label>
                    <input type="text" class="form-control" name="uConsignee" id="uConsignee">
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Update Consignor (SG):</label>
                    <input type="text" class="form-control" name="uConsignor" id="uConsignor">
                  </div>

                  <div class="form-group">
                    <label>Job Type</label>
                    <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="ujobType" id="ujobType">
                      <option value="K1">K1</option>
                      <option value="K2">K2</option>
                      <option value="K8">K8</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>HC & OT</label>
                    <select class="form-control selectpicker show-tick show-menu-arrow" data-header="Select one below..." name="uhandlingMode" id="uhandlingMode">
                      <option value="HC">HC</option>
                      <option value="HC & OT">HC & OT</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Update Remark:</label>
                    <input type="text" class="form-control" name="uRemark" id="uRemark">
                  </div>
                </div>

                <div class="form-group">
                  <button type="button" class="btn btn-primary float-right" onclick="sendUpdate()">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div> <!-- this if for wrapper end -->
