-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 27, 2020 at 10:03 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `citytrans`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobdetails`
--

CREATE TABLE `jobdetails` (
  `id` int(11) NOT NULL,
  `jobID` int(11) NOT NULL,
  `smkReg` varchar(255) NOT NULL,
  `consignee` varchar(255) NOT NULL,
  `consignor` varchar(255) NOT NULL,
  `jobType` varchar(10) NOT NULL,
  `handlingMode` varchar(10) NOT NULL,
  `remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobdetails`
--

INSERT INTO `jobdetails` (`id`, `jobID`, `smkReg`, `consignee`, `consignor`, `jobType`, `handlingMode`, `remark`) VALUES
(1, 2, 'J10105001000', 'ABC', 'ARKEMA', 'K1', 'HC', ''),
(2, 3, 'J10105001002', 'MALAYSIA', 'ARKEMA', 'K1', 'HC', '');

-- --------------------------------------------------------

--
-- Table structure for table `jobhead`
--

CREATE TABLE `jobhead` (
  `id` int(11) NOT NULL,
  `jobDate` date NOT NULL,
  `lorryNo` varchar(10) NOT NULL,
  `cekNo` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobhead`
--

INSERT INTO `jobhead` (`id`, `jobDate`, `lorryNo`, `cekNo`) VALUES
(2, '2020-05-19', 'BLJ8794', 250001),
(3, '2020-05-19', 'BLJ8794', 250001);

-- --------------------------------------------------------

--
-- Table structure for table `lorryselect`
--

CREATE TABLE `lorryselect` (
  `id` int(11) NOT NULL,
  `lorryNo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lorryselect`
--

INSERT INTO `lorryselect` (`id`, `lorryNo`) VALUES
(3, 'BLQ5134'),
(5, 'SWA7618'),
(8, 'BLJ8794');

-- --------------------------------------------------------

--
-- Table structure for table `myselect`
--

CREATE TABLE `myselect` (
  `id` int(11) NOT NULL,
  `myName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `myselect`
--

INSERT INTO `myselect` (`id`, `myName`) VALUES
(7, 'CIPC'),
(8, 'MY'),
(9, 'MY02'),
(10, 'MY02'),
(11, 'MY02'),
(12, 'MY03'),
(13, 'MY04'),
(14, 'ABX'),
(15, 'SIK'),
(17, 'ABC'),
(18, 'ABC1'),
(19, 'ABC2'),
(20, 'ABC3'),
(21, 'ABC31'),
(22, 'ABC33');

-- --------------------------------------------------------

--
-- Table structure for table `sgselect`
--

CREATE TABLE `sgselect` (
  `id` int(11) NOT NULL,
  `sgName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sgselect`
--

INSERT INTO `sgselect` (`id`, `sgName`) VALUES
(2, 'TPSC'),
(3, 'ARKEMA'),
(7, 'ARKEMA1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `avatar`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'jayd.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobdetails`
--
ALTER TABLE `jobdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fk` (`jobID`);

--
-- Indexes for table `jobhead`
--
ALTER TABLE `jobhead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lorryselect`
--
ALTER TABLE `lorryselect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `myselect`
--
ALTER TABLE `myselect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sgselect`
--
ALTER TABLE `sgselect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobdetails`
--
ALTER TABLE `jobdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobhead`
--
ALTER TABLE `jobhead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lorryselect`
--
ALTER TABLE `lorryselect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `myselect`
--
ALTER TABLE `myselect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sgselect`
--
ALTER TABLE `sgselect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobdetails`
--
ALTER TABLE `jobdetails`
  ADD CONSTRAINT `id_fk` FOREIGN KEY (`jobID`) REFERENCES `jobhead` (`id`);
